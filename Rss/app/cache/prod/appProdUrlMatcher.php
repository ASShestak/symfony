<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // rss_news_default_index
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'rss_news_default_index')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/tapes')) {
            // tapes
            if (rtrim($pathinfo, '/') === '/tapes') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_tapes;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'tapes');
                }

                return array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::indexAction',  '_route' => 'tapes',);
            }
            not_tapes:

            // tapes_create
            if ($pathinfo === '/tapes/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_tapes_create;
                }

                return array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::createAction',  '_route' => 'tapes_create',);
            }
            not_tapes_create:

            // tapes_new
            if ($pathinfo === '/tapes/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_tapes_new;
                }

                return array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::newAction',  '_route' => 'tapes_new',);
            }
            not_tapes_new:

            // tapes_show
            if (preg_match('#^/tapes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_tapes_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tapes_show')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::showAction',));
            }
            not_tapes_show:

            // tapes_edit
            if (preg_match('#^/tapes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_tapes_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tapes_edit')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::editAction',));
            }
            not_tapes_edit:

            // tapes_update
            if (preg_match('#^/tapes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_tapes_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tapes_update')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::updateAction',));
            }
            not_tapes_update:

            // tapes_delete
            if (preg_match('#^/tapes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_tapes_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tapes_delete')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\TapesController::deleteAction',));
            }
            not_tapes_delete:

        }

        if (0 === strpos($pathinfo, '/users')) {
            // users
            if (rtrim($pathinfo, '/') === '/users') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_users;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'users');
                }

                return array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::indexAction',  '_route' => 'users',);
            }
            not_users:

            // users_create
            if ($pathinfo === '/users/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_users_create;
                }

                return array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::createAction',  '_route' => 'users_create',);
            }
            not_users_create:

            // users_new
            if ($pathinfo === '/users/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_users_new;
                }

                return array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::newAction',  '_route' => 'users_new',);
            }
            not_users_new:

            // users_show
            if (preg_match('#^/users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_users_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_show')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::showAction',));
            }
            not_users_show:

            // users_edit
            if (preg_match('#^/users/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_users_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_edit')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::editAction',));
            }
            not_users_edit:

            // users_update
            if (preg_match('#^/users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_users_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_update')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::updateAction',));
            }
            not_users_update:

            // users_delete
            if (preg_match('#^/users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_users_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_delete')), array (  '_controller' => 'Rss\\NewsBundle\\Controller\\UsersController::deleteAction',));
            }
            not_users_delete:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
