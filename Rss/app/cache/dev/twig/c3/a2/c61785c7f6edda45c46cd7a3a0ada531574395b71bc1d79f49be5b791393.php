<?php

/* RssNewsBundle:News:index.html.twig */
class __TwigTemplate_c3a2c61785c7f6edda45c46cd7a3a0ada531574395b71bc1d79f49be5b791393 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "    <br>
";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "    <div id=\"item\">
        <ul>
            ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 11
            echo "                    <li>
                        <a  href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_load", array("category" => (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")))), "html", null, true);
            echo "\"><em>";
            echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
            echo "</em></a>
                    </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "        </ul>
    </div>
    <br>
    <form action=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("news");
        echo "\" method=\"get\" class=\"form-inline\" role=\"form\">
        <div class=\"row\">
            <div class=\"span3 thumb-list\">
                ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["search_form"]) ? $context["search_form"] : $this->getContext($context, "search_form")), "find"), 'row');
        echo "
            </div>
        </div>
    </form>

    <hr>
    ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rss"]) ? $context["rss"] : $this->getContext($context, "rss")));
        foreach ($context['_seq'] as $context["_key"] => $context["tape"]) {
            // line 28
            echo "        <div class=\"row\">
            <div class=\"span9 thumb-list\">
                    <img src=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/images/rssicon1.png"), "html", null, true);
            echo "\" class=\"news_img\">
                    <a rel=\"lightbox[secondary]\" href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_show", array("id" => $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "id"))), "html", null, true);
            echo "\">
                        ";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "title"), "html", null, true);
            echo "
                    </a>
            </div>
            <div class=\"span2 thumb-list\">
                <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/images/calendar.png"), "html", null, true);
            echo "\" class=\"news_img\">
                ";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "dataUpdate"), "Y-m-d"), "html", null, true);
            echo "
            </div>
            <div class=\"span1 thumb-list\">
                <em class=\"text-right\">
                    <img src=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/images/user_foto.png"), "html", null, true);
            echo "\" class=\"news_img\" >
                    (";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "visits"), "html", null, true);
            echo ")
                </em>
            </div>
        </div>
        <hr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tape'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "    ";
        echo $this->env->getExtension('knp_pagination')->render((isset($context["rss"]) ? $context["rss"] : $this->getContext($context, "rss")));
        echo "
";
    }

    // line 51
    public function block_script($context, array $blocks = array())
    {
        // line 52
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/jquery-1.3.2.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/3DEngine.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/Ring.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
    <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/func.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
";
    }

    public function getTemplateName()
    {
        return "RssNewsBundle:News:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 53,  110 => 37,  129 => 50,  65 => 13,  152 => 54,  113 => 23,  90 => 16,  70 => 5,  58 => 41,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 51,  132 => 31,  128 => 29,  107 => 36,  61 => 13,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 52,  135 => 53,  119 => 42,  102 => 32,  71 => 19,  67 => 15,  63 => 15,  59 => 14,  38 => 7,  94 => 28,  89 => 26,  85 => 25,  75 => 7,  68 => 18,  56 => 9,  87 => 28,  21 => 2,  26 => 6,  93 => 28,  88 => 6,  78 => 8,  46 => 8,  27 => 1,  44 => 12,  31 => 5,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 55,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  121 => 42,  117 => 41,  105 => 21,  91 => 30,  62 => 12,  49 => 11,  24 => 4,  25 => 3,  19 => 1,  79 => 18,  72 => 16,  69 => 25,  47 => 9,  40 => 7,  37 => 6,  22 => 2,  246 => 90,  157 => 40,  145 => 46,  139 => 35,  131 => 52,  123 => 46,  120 => 40,  115 => 43,  111 => 37,  108 => 36,  101 => 33,  98 => 19,  96 => 18,  83 => 27,  74 => 21,  66 => 16,  55 => 40,  52 => 12,  50 => 16,  43 => 7,  41 => 8,  35 => 3,  32 => 3,  29 => 2,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 45,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 48,  130 => 41,  125 => 44,  122 => 43,  116 => 41,  112 => 42,  109 => 38,  106 => 36,  103 => 32,  99 => 32,  95 => 31,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 45,  60 => 14,  57 => 12,  54 => 11,  51 => 14,  48 => 9,  45 => 10,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 3,);
    }
}
