<?php

/* ::base.html.twig */
class __TwigTemplate_6832ff42cec9ae595968938477dccf791ba0d1b362a733ee11fae9749c819297 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'head' => array($this, 'block_head'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <title>
        ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "    </title>
    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 11
        echo "</head>
<body>
<div class=\"container\" >
    <div class=\"container-narrow\">
        <div id=\"container\">
        ";
        // line 16
        $this->displayBlock('head', $context, $blocks);
        // line 39
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 40
        echo "        ";
        $this->displayBlock('footer', $context, $blocks);
        // line 41
        echo "        </div>
    </div>
</div>
</body>
";
        // line 45
        $this->displayBlock('script', $context, $blocks);
        // line 46
        echo "</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/css/bootstrap.css"), "html", null, true);
        echo "\">
    ";
    }

    // line 10
    public function block_javascripts($context, array $blocks = array())
    {
    }

    // line 16
    public function block_head($context, array $blocks = array())
    {
        // line 17
        echo "        <div class=\"masthead\">
            ";
        // line 18
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 19
            echo "            <ul class=\"nav nav-pills pull-right\">
                <li><a href=\"";
            // line 20
            echo $this->env->getExtension('routing')->getPath("tape");
            echo "\">Rss</a></li>
                <li><a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("user");
            echo "\">User</a></li>
                <li><a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("category");
            echo "\">Category</a></li>
                <li><a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("news");
            echo "\">News</a></li>
                <li><a href=\"";
            // line 24
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\">
                <img src=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/images/System-Logout-icon.png"), "html", null, true);
            echo "\" class=\"news_log\" >
                        Log Out</a><li>
            </ul>
            ";
        } else {
            // line 29
            echo "                <ul class=\"nav nav-pills pull-right\">
                    <li>
                        <a href=\"";
            // line 31
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\">Log In</a>
                    <li>
                </ul>
            ";
        }
        // line 35
        echo "            <h3 class=\"muted\"> ";
        $this->displayBlock('header', $context, $blocks);
        echo "</h3>
        </div>
        <br>
        ";
    }

    public function block_header($context, array $blocks = array())
    {
    }

    // line 39
    public function block_body($context, array $blocks = array())
    {
    }

    // line 40
    public function block_footer($context, array $blocks = array())
    {
    }

    // line 45
    public function block_script($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  152 => 39,  113 => 23,  90 => 16,  70 => 5,  58 => 41,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 31,  128 => 29,  107 => 36,  61 => 13,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  119 => 42,  102 => 32,  71 => 19,  67 => 15,  63 => 15,  59 => 14,  38 => 7,  94 => 28,  89 => 20,  85 => 10,  75 => 7,  68 => 14,  56 => 9,  87 => 25,  21 => 2,  26 => 6,  93 => 17,  88 => 6,  78 => 8,  46 => 7,  27 => 1,  44 => 12,  31 => 5,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  121 => 25,  117 => 24,  105 => 21,  91 => 27,  62 => 23,  49 => 19,  24 => 4,  25 => 3,  19 => 1,  79 => 18,  72 => 16,  69 => 25,  47 => 9,  40 => 10,  37 => 5,  22 => 2,  246 => 90,  157 => 40,  145 => 46,  139 => 35,  131 => 52,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 36,  101 => 20,  98 => 19,  96 => 18,  83 => 25,  74 => 14,  66 => 46,  55 => 40,  52 => 39,  50 => 16,  43 => 11,  41 => 7,  35 => 6,  32 => 3,  29 => 2,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 45,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 41,  112 => 42,  109 => 22,  106 => 36,  103 => 32,  99 => 31,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 45,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 7,  42 => 7,  39 => 9,  36 => 5,  33 => 5,  30 => 7,);
    }
}
