<?php

/* KnpPaginatorBundle:Pagination:sortable_link.html.twig */
class __TwigTemplate_11f5680754082971ebeb978f251a0189fa1c10cc11172cd7467a699a5d2a2fea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")));
        foreach ($context['_seq'] as $context["attr"] => $context["value"]) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attr'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "KnpPaginatorBundle:Pagination:sortable_link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 49,  114 => 39,  100 => 33,  76 => 22,  97 => 32,  181 => 73,  118 => 46,  153 => 59,  150 => 53,  127 => 43,  126 => 34,  148 => 53,  110 => 33,  129 => 41,  65 => 20,  152 => 54,  113 => 34,  90 => 30,  70 => 5,  58 => 13,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 51,  132 => 48,  128 => 48,  107 => 32,  61 => 12,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 62,  143 => 41,  135 => 39,  119 => 36,  102 => 32,  71 => 19,  67 => 15,  63 => 14,  59 => 18,  38 => 10,  94 => 30,  89 => 28,  85 => 22,  75 => 18,  68 => 15,  56 => 9,  87 => 24,  21 => 2,  26 => 6,  93 => 28,  88 => 29,  78 => 8,  46 => 13,  27 => 5,  44 => 5,  31 => 5,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 68,  166 => 71,  163 => 62,  158 => 55,  156 => 55,  151 => 63,  142 => 42,  138 => 40,  136 => 56,  121 => 37,  117 => 40,  105 => 35,  91 => 29,  62 => 19,  49 => 10,  24 => 4,  25 => 3,  19 => 1,  79 => 25,  72 => 17,  69 => 18,  47 => 6,  40 => 6,  37 => 5,  22 => 3,  246 => 90,  157 => 40,  145 => 46,  139 => 41,  131 => 36,  123 => 44,  120 => 40,  115 => 41,  111 => 37,  108 => 36,  101 => 30,  98 => 35,  96 => 29,  83 => 26,  74 => 16,  66 => 15,  55 => 17,  52 => 16,  50 => 9,  43 => 12,  41 => 11,  35 => 3,  32 => 7,  29 => 6,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 71,  173 => 65,  168 => 72,  164 => 59,  162 => 56,  154 => 58,  149 => 51,  147 => 56,  144 => 49,  141 => 46,  133 => 43,  130 => 41,  125 => 44,  122 => 47,  116 => 41,  112 => 45,  109 => 44,  106 => 30,  103 => 34,  99 => 28,  95 => 27,  92 => 26,  86 => 27,  82 => 18,  80 => 21,  73 => 23,  64 => 45,  60 => 14,  57 => 11,  54 => 11,  51 => 14,  48 => 7,  45 => 7,  42 => 7,  39 => 4,  36 => 3,  33 => 5,  30 => 2,);
    }
}
