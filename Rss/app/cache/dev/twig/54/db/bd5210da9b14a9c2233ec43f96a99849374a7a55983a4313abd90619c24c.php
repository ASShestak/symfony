<?php

/* KnpPaginatorBundle:Pagination:filtration.html.twig */
class __TwigTemplate_54dbbd5210da9b14a9c2233ec43f96a99849374a7a55983a4313abd90619c24c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"get\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")), "html", null, true);
        echo "\" enctype=\"application/x-www-form-urlencoded\">

    <select name=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["filterFieldName"]) ? $context["filterFieldName"] : $this->getContext($context, "filterFieldName")), "html", null, true);
        echo "\">
        ";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) ? $context["fields"] : $this->getContext($context, "fields")));
        foreach ($context['_seq'] as $context["field"] => $context["label"]) {
            // line 5
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, (isset($context["field"]) ? $context["field"] : $this->getContext($context, "field")), "html", null, true);
            echo "\"";
            if (((isset($context["selectedField"]) ? $context["selectedField"] : $this->getContext($context, "selectedField")) == (isset($context["field"]) ? $context["field"] : $this->getContext($context, "field")))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['field'], $context['label'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "    </select>

    <input type=\"text\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["selectedValue"]) ? $context["selectedValue"] : $this->getContext($context, "selectedValue")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["filterValueName"]) ? $context["filterValueName"] : $this->getContext($context, "filterValueName")), "html", null, true);
        echo "\" />

    <button>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "button"), "html", null, true);
        echo "</button>

</form>";
    }

    public function getTemplateName()
    {
        return "KnpPaginatorBundle:Pagination:filtration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 49,  114 => 34,  100 => 36,  76 => 22,  97 => 32,  181 => 73,  118 => 46,  153 => 59,  150 => 53,  127 => 43,  126 => 34,  148 => 53,  110 => 33,  129 => 41,  65 => 13,  152 => 54,  113 => 34,  90 => 30,  70 => 5,  58 => 13,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 51,  132 => 48,  128 => 48,  107 => 32,  61 => 12,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 62,  143 => 41,  135 => 39,  119 => 36,  102 => 32,  71 => 19,  67 => 15,  63 => 14,  59 => 11,  38 => 4,  94 => 28,  89 => 27,  85 => 22,  75 => 18,  68 => 15,  56 => 9,  87 => 24,  21 => 2,  26 => 6,  93 => 28,  88 => 29,  78 => 8,  46 => 6,  27 => 1,  44 => 5,  31 => 5,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 68,  166 => 71,  163 => 62,  158 => 55,  156 => 55,  151 => 63,  142 => 42,  138 => 40,  136 => 56,  121 => 37,  117 => 40,  105 => 37,  91 => 21,  62 => 12,  49 => 10,  24 => 4,  25 => 3,  19 => 1,  79 => 17,  72 => 17,  69 => 18,  47 => 6,  40 => 6,  37 => 5,  22 => 2,  246 => 90,  157 => 40,  145 => 46,  139 => 41,  131 => 36,  123 => 44,  120 => 40,  115 => 41,  111 => 37,  108 => 36,  101 => 30,  98 => 35,  96 => 29,  83 => 26,  74 => 16,  66 => 15,  55 => 9,  52 => 9,  50 => 9,  43 => 5,  41 => 8,  35 => 3,  32 => 3,  29 => 4,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 71,  173 => 65,  168 => 72,  164 => 59,  162 => 56,  154 => 58,  149 => 51,  147 => 56,  144 => 49,  141 => 46,  133 => 43,  130 => 41,  125 => 44,  122 => 47,  116 => 41,  112 => 45,  109 => 44,  106 => 30,  103 => 31,  99 => 28,  95 => 27,  92 => 26,  86 => 28,  82 => 18,  80 => 21,  73 => 19,  64 => 45,  60 => 14,  57 => 11,  54 => 11,  51 => 14,  48 => 7,  45 => 7,  42 => 7,  39 => 4,  36 => 3,  33 => 5,  30 => 2,);
    }
}
