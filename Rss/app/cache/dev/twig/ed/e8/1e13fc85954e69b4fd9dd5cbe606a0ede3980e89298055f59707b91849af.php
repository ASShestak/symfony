<?php

/* RssNewsBundle:News:show.html.twig */
class __TwigTemplate_ede81e13fc85954e69b4fd9dd5cbe606a0ede3980e89298055f59707b91849af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_header($context, array $blocks = array())
    {
        // line 3
        echo "    Новости ленты:
";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "
    (<a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("news");
        echo "\">Вернуться к списку</a>)
    <div id = \"data\" data-url=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_show", array("id" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))), "html", null, true);
        echo "\" ></div>
    <div id=\"loading\" data-img=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/images/loading.gif"), "html", null, true);
        echo "\"></div>
    <hr>
        ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tapes"]) ? $context["tapes"] : $this->getContext($context, "tapes")));
        foreach ($context['_seq'] as $context["_key"] => $context["tape"]) {
            // line 12
            echo "        <div class=\"row\">
            <div class=\"span12 thumb-list\">
                <h4><a rel=\"lightbox[secondary]\" href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "link"), "html", null, true);
            echo "\">
                        ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "title"), "html", null, true);
            echo "
                    </a></h4>
                <p>";
            // line 17
            echo $this->getAttribute((isset($context["tape"]) ? $context["tape"] : $this->getContext($context, "tape")), "description");
            echo "</p>
            </div>
         </div>
         <hr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tape'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    <div id=\"container\">
        <div class=\"pagination\">
            <ul>
                ";
        // line 25
        if (((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) != 1)) {
            // line 26
            echo "                <li p='1' class='active'>  First  </li>
                <li p='";
            // line 27
            echo twig_escape_filter($this->env, ((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) - 1), "html", null, true);
            echo "' class='active'>  Prev  </li>
                <li p='";
            // line 28
            echo twig_escape_filter($this->env, ((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) - 1), "html", null, true);
            echo "' class='active'>  ";
            echo twig_escape_filter($this->env, ((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) - 1), "html", null, true);
            echo "  </li>
                ";
        }
        // line 30
        echo "                <li p='";
        echo twig_escape_filter($this->env, (isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")), "html", null, true);
        echo "' class='noactive'>  ";
        echo twig_escape_filter($this->env, (isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")), "html", null, true);
        echo "  </li>
                ";
        // line 31
        if (((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) < (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")))) {
            // line 32
            echo "                    <li p='";
            echo twig_escape_filter($this->env, ((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) + 1), "html", null, true);
            echo "' class='active'>  ";
            echo twig_escape_filter($this->env, ((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) + 1), "html", null, true);
            echo "  </li>
                    <li p='";
            // line 33
            echo twig_escape_filter($this->env, ((isset($context["pageCarrent"]) ? $context["pageCarrent"] : $this->getContext($context, "pageCarrent")) + 1), "html", null, true);
            echo "' class='active'>  Next  </li>
                    <li p='";
            // line 34
            echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "html", null, true);
            echo "' class='active'>  Last  </li>
                ";
        }
        // line 36
        echo "            </ul>
        </div>
    </div>

";
    }

    // line 41
    public function block_script($context, array $blocks = array())
    {
        // line 42
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/jquery-1.4.2.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
    <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/rssnews/js/page.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
";
    }

    public function getTemplateName()
    {
        return "RssNewsBundle:News:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 34,  148 => 53,  110 => 30,  129 => 50,  65 => 14,  152 => 54,  113 => 31,  90 => 25,  70 => 5,  58 => 41,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 51,  132 => 31,  128 => 35,  107 => 36,  61 => 12,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 41,  135 => 39,  119 => 32,  102 => 32,  71 => 19,  67 => 15,  63 => 14,  59 => 12,  38 => 5,  94 => 28,  89 => 25,  85 => 22,  75 => 7,  68 => 18,  56 => 9,  87 => 24,  21 => 2,  26 => 6,  93 => 28,  88 => 6,  78 => 8,  46 => 8,  27 => 1,  44 => 7,  31 => 5,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 55,  151 => 63,  142 => 42,  138 => 40,  136 => 56,  121 => 42,  117 => 41,  105 => 21,  91 => 30,  62 => 12,  49 => 11,  24 => 4,  25 => 3,  19 => 1,  79 => 18,  72 => 17,  69 => 15,  47 => 9,  40 => 7,  37 => 6,  22 => 2,  246 => 90,  157 => 40,  145 => 46,  139 => 41,  131 => 36,  123 => 33,  120 => 40,  115 => 32,  111 => 37,  108 => 36,  101 => 33,  98 => 19,  96 => 27,  83 => 22,  74 => 17,  66 => 16,  55 => 11,  52 => 9,  50 => 9,  43 => 7,  41 => 6,  35 => 3,  32 => 3,  29 => 2,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 45,  154 => 58,  149 => 51,  147 => 43,  144 => 49,  141 => 48,  133 => 48,  130 => 41,  125 => 44,  122 => 33,  116 => 41,  112 => 31,  109 => 38,  106 => 30,  103 => 29,  99 => 28,  95 => 27,  92 => 26,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 45,  60 => 14,  57 => 11,  54 => 11,  51 => 14,  48 => 8,  45 => 10,  42 => 7,  39 => 9,  36 => 5,  33 => 3,  30 => 2,);
    }
}
