$(document).ready(function(){
    // Get items
    function getItems(exampleNr)
    {
        var columns = [];
        $(exampleNr + ' ul.sortable-list').each(function(){
            columns.push($(this).sortable('toArray').join('|'));
        });
        var myData = columns[0];
        var obj=document.getElementById("rss_newsbundle_tape_category");
        obj.value=myData;
    }

    $('#category .sortable-list').sortable({
        connectWith: '#category .sortable-list',
        update: function (){
            getItems('#category');
        }
    });

    getItems('#category');

});

