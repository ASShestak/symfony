<?php

namespace Rss\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TapeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('url',null,array('label'=>'RssLink'))
            ->add('category', 'hidden', array('required' => false,'mapped' => false))
            ->add('active');

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rss\NewsBundle\Entity\Tape'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rss_newsbundle_tape';
    }


}
