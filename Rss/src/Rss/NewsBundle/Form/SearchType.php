<?php
namespace Rss\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;


class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('search', 'search', array(
                     'required' => false,
                     'label' => 'Title',
                     'attr'=>array('class'=>"form-control", 'id'=>"exampleInputEmail2")))
                ->add('sort', 'choice', array(
                     'required' => false,
                     'choices'   => array('visits'=>'Visits','dataCreate'=>'Create')))
                ->add('dateFrom', 'date', array(
                    'label' => 'From',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'attr' => array('class' => 'date'),
                    'required' => false))
                ->add('dateTo', 'date', array(
                    'label' => 'To',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'attr' => array('class' => 'date'),
                    'required' => false))
                ->add('find', 'search', array(
                    'required' => false,
                    'label' => false,
                    'attr'=>array('class'=>"form-control", 'id'=>"exampleInputEmail2", 'placeholder' => "Поиск Rss...")));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'csrf_protection' => false,
        );
    }

    function getName()
    {
        return 'searchorg';
    }
}