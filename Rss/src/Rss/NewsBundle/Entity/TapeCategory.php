<?php

namespace Rss\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TapeCategory
 *
 * @ORM\Table(name="tape_category")
 * @ORM\Entity
 */
class TapeCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Tape
     *
     * @ORM\ManyToOne(targetEntity="Tape")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tape_id", referencedColumnName="id")
     * })
     */
    private $tape;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tape
     *
     * @param \Rss\NewsBundle\Entity\Tape $tape
     * @return TapeCategory
     */
    public function setTape(\Rss\NewsBundle\Entity\Tape $tape = null)
    {
        $this->tape = $tape;
    
        return $this;
    }

    /**
     * Get tape
     *
     * @return \Rss\NewsBundle\Entity\Tape
     */
    public function getTape()
    {
        return $this->tape;
    }

    /**
     * Set category
     *
     * @param \Rss\NewsBundle\Entity\Category $category
     * @return TapeCategory
     */
    public function setCategory(\Rss\NewsBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Rss\NewsBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

}