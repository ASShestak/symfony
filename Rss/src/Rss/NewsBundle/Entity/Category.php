<?php

namespace Rss\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = "3", max = "15",
     *                minMessage = "Name must have at least 3 characters.",
     *                maxMessage = "Name must have at more 15 characters.")
     * @Assert\Regex(
     *     pattern="/^[A-Za-zА-Яа-я0-9 ]+$/ui",
     *     message="Your name cannot contain a number and other character!")
     */
    private $title;

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /*public function __toString()
    {
        return $this->title;
    }*/

}