<?php

namespace Rss\NewsBundle\Entity;

use Rss\NewsBundle\Entity\Entity;
use Doctrine\ORM\EntityRepository;

class TapeRepository extends EntityRepository
{
    public function findTape( $em, $search, $dateFrom, $dateTo, $sort )
    {
        $qb = $em->getRepository('RssNewsBundle:Tape')->createQueryBuilder('n');
        $qb->select('n');
        if (isset($search))
            $qb->where("n.title LIKE '%". $search."%'");
        if ($dateFrom)
            $qb->andWhere($qb->expr()->gte('n.dataUpdate',
                $qb->expr()->literal($dateFrom->format('Y-m-d'))));
        if ($dateTo)
            $qb->andWhere($qb->expr()->lte('n.dataUpdate',
                $qb->expr()->literal($dateTo->format('Y-m-d'))));
        if ($sort) {
            $qb->orderBy('n.'.$sort,'DESC');
        } else {
            $qb->orderBy('n.dataUpdate', 'DESC');
        }
        return $qb;
    }

    public function findRss( $em, $search)
    {
        $qb = $em->getRepository('RssNewsBundle:Tape')->createQueryBuilder('n');
        $qb->select('n');
        if (isset($search))
            $qb->where("n.title LIKE '%". $search."%'");
        $qb->andwhere('n.active = 1 ');
        $qb->orderBy('n.visits',' DESC');
        return $qb;
    }

}
