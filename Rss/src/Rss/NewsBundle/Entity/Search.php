<?php
namespace Rss\NewsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Search
{
    /**
     * Строка поиска
     * @var string
     */
    public $search;

    /**
     * Строка поиска
     * @var string
     */
    public $find;

    /**
     * Строка поиска
     * @var string
     */
    public $sort;

    /**
     * Дата с которой искать новости
     * @var DateTime
     * @Assert\DateTime
     */
    public $dateFrom;

    /**
     * Дата, до которой искать новости
     * @var DateTime
     * @Assert\DateTime
     */
    public $dateTo;
}