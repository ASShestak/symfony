<?php

namespace Rss\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tape
 *
 * @ORM\Table(name="tape")
 * @ORM\Entity(repositoryClass="Rss\NewsBundle\Entity\TapeRepository")
 *
 */
class Tape
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = "3", max = "25",
     *                minMessage = "Name must have at least 3 characters.",
     *                maxMessage = "Name must have at more 25 characters.")
     * @Assert\Regex(
     *     pattern="/^[A-Za-zА-Яа-я0-9 ]+$/ui",
     *     message="Your name cannot contain a number and other character!")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=45, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = "10", max = "45",
     *                minMessage = "Name must have at least 10 characters.",
     *                maxMessage = "Name must have at more 45 characters.")
     *
     */
    private $url;

    /**
     * @var \Date
     *
     * @ORM\Column(name="data_create", type="date", nullable=false)
     *
     */
    private $dataCreate;

    /**
     * @var \Date
     *
     * @ORM\Column(name="data_update", type="date", nullable=false)
     */
    private $dataUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="visits", type="integer", nullable=true)
     */
    private $visits;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @ORM\OrderBy({"visits" = "DESC"})
     */
    private $active;


    public function __construct(){

        $this->dataCreate = date_create();
        $this->dataUpdate = date_create();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Tape
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Tape
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set dataCreate
     *
     * @param \Date $dataCreate
     * @return Tape
     */
    public function setDataCreate($dataCreate)
    {
        $this->dataCreate = $dataCreate;
    
        return $this;
    }

    /**
     * Get dataCreate
     *
     * @return \Date
     */
    public function getDataCreate()
    {
        return $this->dataCreate;
    }

    /**
     * Set dataUpdate
     *
     * @param \Date $dataUpdate
     * @return Tape
     */
    public function setDataUpdate($dataUpdate)
    {
        $this->dataUpdate = $dataUpdate;
    
        return $this;
    }

    /**
     * Get dataUpdate
     *
     * @return \Date
     */
    public function getDataUpdate()
    {
        return $this->dataUpdate;
    }

    /**
     * Set visits
     *
     * @param integer $visits
     * @return Tape
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;
    
        return $this;
    }

    /**
     * Get visits
     *
     * @return integer 
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * Set active
     *
     * @param string $active
     * @return Tape
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

}