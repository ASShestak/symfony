<?php

namespace Rss\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = "3", max = "15",
     *                minMessage = "Name must have at least 3 characters.",
     *                maxMessage = "Name must have at more 15 characters.")
     * @Assert\Regex(
     *     pattern="/^[A-Za-z]+$/",
     *     message="Your name cannot contain a number and other character!")
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = "5", max = "15",
     *                minMessage = "Password must have at least 5 characters.",
     *                maxMessage = "Password must have at more 15 characters.")
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9]+$/",
     *     message="Your name cannot contain a number and other character!")
     */
    private $password;

    /**
     * @ORM\Column(name="salt", type="string", length=45, nullable=false)
     *
     * @var string salt
     */
    protected $salt;

    /**
     * @ORM\Column(name="role", type="string", length=45, nullable=false)
     *
     * @var string role
     */
    protected $role;

    public function __construct()
    {
        $this->role = 'ROLE_ADMIN';
        $this->salt = md5('admin');
    }

    /**
     * Get id
     *
     * @return integer 
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }
    public function setRoles($role)
    {
        $this->role = $role;
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getRoles()
    {
        return array($this->role);
    }


    public function __toString()
    {
        return $this->username;
    }

}