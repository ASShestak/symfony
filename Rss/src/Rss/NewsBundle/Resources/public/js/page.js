$(document).ready(function(){
    function loadData(page){
        $('#loading').html("<img src='"+$('#loading').attr('data-img')+"'/>").fadeIn('fast');
        $.ajax
        ({
            type: "GET",
            url: $('#data').attr('data-url'),
            data: page,
            success: function(msg)
            {
                $('#loading').fadeOut('fast');
                $("#container").html(msg);
            }
        });
    }

    $('#container .pagination li.active').live('click',function(){
        var page = 'page='+$(this).attr('p');
        loadData(page);
    });
});