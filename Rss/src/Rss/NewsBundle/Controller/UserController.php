<?php

namespace Rss\NewsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Rss\NewsBundle\Entity\User;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RssNewsBundle:User')->findAll();
        return array('entities' => $entities);
    }

    /**
     * @Route("/new", name="user_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $user = new User();
        /** @var $rss \Rss\NewsBundle\RssService\FormService */
        $rss = $this->get('rss_form');
        $form = $rss->createUserForm($user);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('RssNewsBundle:User:')->findOneByUsername($user->getUsername());
                if (!$entity) {
                    $factory = $this->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    $password = $encoder->encodePassword(
                        $user->getPassword(),
                        $user->getSalt()
                    );
                    $user->setPassword( $password);
                    $em->persist($user);
                    $em->flush();
                    $this->get('session')->getFlashbag()->add(
                        'success_message',
                        "User was added."
                    );
                    return $this->redirect($this->generateUrl('user'));
                } else {
                    $this->get('session')->getFlashbag()->add(
                        'error_message',
                        "User already exists."
                    );
                }
            }
        }
        return array(
            'entity' => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="user_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RssNewsBundle:User')->findAll();
        if (count($entities)!=1) {
            $user = $em->getRepository('RssNewsBundle:User')->find($id);
            $em->remove($user);
            $em->flush();
            $this->get('session')->getFlashbag()->add(
                'warning_message',
                'User was deleted.'
            );
        } else {
            $this->get('session')->getFlashbag()->add(
                'error_message',
                'User was not deleted. Last user .');
        }
        return $this->redirect($this->generateUrl('user'));
    }
}
