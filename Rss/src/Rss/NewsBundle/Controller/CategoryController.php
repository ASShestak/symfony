<?php

namespace Rss\NewsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Rss\NewsBundle\Entity\Category;

/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="category")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RssNewsBundle:Category')->findAll();
        return array('entities' => $entities);
    }

    /**
     * @Route("/new", name="category_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        /** @var $rss \Rss\NewsBundle\RssService\FormService */
        $rss = $this->get('rss_form');
        $form = $rss->createCategoryForm($category);
        if ($request->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('RssNewsBundle:Category:')->findOneByTitle( $category->getTitle());
                if (!$entity) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($category);
                    $em->flush();
                    $this->get('session')->getFlashbag()->add(
                        'success_message',
                        'Category was added.'
                    );
                    return $this->redirect($this->generateUrl('category'));
                } else {
                    $this->get('session')->getFlashbag()->add(
                        'error_message',
                        "Category already exists."
                    );
                }
            }
        }
        return array(
            'entity' =>  $category,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", name="category_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('RssNewsBundle:Category')->find($id);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        /** @var $rss \Rss\NewsBundle\RssService\FormService */
        $rss = $this->get('rss_form');
        $form = $rss->createCategoryForm($category);
        if ($request->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            if ($form->isValid()) {
                $em->flush();
                $this->get('session')->getFlashbag()->add(
                    'success_message',
                    'Category is update.'
                );
                return $this->redirect($this->generateUrl('category_edit', array('id' => $id)));
            }
        }
        return array(
            'entity'      => $category,
            'edit_form'   => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="category_delete")
     */
    public function deleteAction( $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RssNewsBundle:Category')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashbag()->add(
            'warning_message',
            'Category was deleted.'
        );
        return $this->redirect($this->generateUrl('category'));
    }
}
