<?php

namespace Rss\NewsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Rss\NewsBundle\Entity\Tape;
use Rss\NewsBundle\Entity\Search as Search;
use Rss\NewsBundle\Form\SearchType as SearchType;
/**
 * Tape controller.
 *
 * @Route("/tape")
 */
class TapeController extends Controller
{

    /**
     * @Route("/", name="tape")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $search = new Search();
        $searchForm = $this->createForm(new SearchType, $search);
        $searchForm->submit($this->getRequest());
        $em = $this->getDoctrine()->getManager();

        /** @var $entities \Rss\NewsBundle\Entity\TapeRepository */
        $entities = $em->getRepository('RssNewsBundle:Tape');
        $qb=$entities->findTape($em, $search->search, $search->dateFrom, $search->dateTo, $search->sort);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $qb->getQuery(),
            $this->get('request')->query->get('page', 1), 5);
        return array(
            'entities' => $pagination,'search_form' => $searchForm->createView(),
        );
    }

    /**
     * @Route("/new", name="tape_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $tape = new Tape();
        /** @var $rss \Rss\NewsBundle\RssService\FormService*/
        $rss = $this->get('rss_form');
        $form = $rss->createTapeForm($tape);
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $form->handleRequest($this->getRequest());


            if ($form->isValid() && $rss->checkRss($tape->getUrl())) {
                $em->persist($tape);
                $em->flush();
                if ($form->get('category')->getData()) {
                    $entityTape = $em->getRepository('RssNewsBundle:Tape')->find($tape->getId());
                    /** @var $serviceTape \Rss\NewsBundle\RssService\TapeService*/
                    $serviceTape = $this->get('rss_tape');
                    $serviceTape->saveCategory($form->get('category')->getData(),$entityTape);
                }
                $this->get('session')->getFlashbag()->add(
                    'success_message',
                    'RssLink is added.'
                );
                return $this->redirect($this->generateUrl('tape_show', array('id' => $tape->getId())));
            }
        }

        $categories = $em->getRepository('RssNewsBundle:Category')->findAll();

        return array(
            'entity'   => $tape,
            'form'     => $form->createView(),
            'category' => $categories,
        );
    }

    /**
     * @Route("/{id}", name="tape_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RssNewsBundle:Tape')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tape entity.');
        }
        $tapeCategories = $em->getRepository('RssNewsBundle:TapeCategory')->findByTape($id);
        return array(
            'entity'         => $entity,
            'tapeCategories' =>$tapeCategories
        );
    }

    /**
     * @Route("/{id}/edit", name="tape_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $tape = $em->getRepository('RssNewsBundle:Tape')->find($id);
        /** @var $rss \Rss\NewsBundle\RssService\FormService */
        $rss = $this->get('rss_form');
        $form = $rss->createTapeForm($tape);

        if (!$tape) {
            throw $this->createNotFoundException('Unable to find Tape entity.');
        }

        if ($request->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            if ($form->isValid()&& $rss->checkRss($tape->getUrl())) {
                $tape->setDataUpdate(date_create());
                $em->flush();
                /** @var $serviceTape \Rss\NewsBundle\RssService\TapeService*/
                $serviceTape = $this->get('rss_tape');
                $serviceTape->deleteCategory($id);
                if ($form->get('category')->getData()) {
                    $serviceTape->saveCategory($form->get('category')->getData(),$tape);
                }
                $this->get('session')->getFlashbag()->add(
                    'success_message',
                    'Rss updated.'
                );
                return $this->redirect($this->generateUrl('tape_edit', array('id' => $id)));
            }
        }
        $loadCategories = array();
        $tapeCategories = $em->getRepository('RssNewsBundle:TapeCategory')->findByTape($id);
        foreach ($tapeCategories as $value) {
            array_push($loadCategories, $value->getCategory()->getId());
        }

        $allCategories = $em->getRepository('RssNewsBundle:Category')->findAll();
        $idCategories = array();
        foreach ($allCategories as $value) {
            array_push($idCategories, $value->getId());
        }

        $categories = array();
        foreach (array_diff( $idCategories, $loadCategories) as $value) {
            array_push($categories, $em->getRepository('RssNewsBundle:Category')->find($value));
        }

        return array(
            'entity'      => $tape,
            'edit_form'   => $form->createView(),
            'category'    => $categories,
            'tapeCategory'=> $tapeCategories
        );
    }

    /**
     * @Route("/delete/{id}", name="tape_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RssNewsBundle:Tape')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tape entity.');
        }

        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashbag()->add(
            'warning_message',
            'RssLink is deleted.'
         );
        return $this->redirect($this->generateUrl('tape'));
    }
}
