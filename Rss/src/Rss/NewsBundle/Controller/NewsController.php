<?php
namespace Rss\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Rss\NewsBundle\Entity\Search as Search;
use Rss\NewsBundle\Form\SearchType as SearchType;


/**
 * @Route("/")
 */
class NewsController extends Controller
{
    /**
     * @Route("/", name="news")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $search = new Search();
        $searchForm = $this->createForm(new SearchType, $search);
        $searchForm->submit($this->getRequest());
        $em = $this->getDoctrine()->getManager();
        /** @var $rss \Rss\NewsBundle\RssService\TapeService */
        $rss = $this->get('rss_tape');
        $categories = $rss->loadCategory();

        /** @var $entities \Rss\NewsBundle\Entity\TapeRepository */
        $entities = $em->getRepository('RssNewsBundle:Tape');
        $qb=$entities->findRss($em, $search->find);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $qb->getQuery(),
        $this->get('request')->query->get('page', 1), 5);
        return array(
            'rss' => $pagination,'categories'=>$categories,'search_form' => $searchForm->createView(),
        );
    }


    /**
     * @Route("/news/{id}", name="news_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $news = array();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RssNewsBundle:Tape')->find($id);
        $url = $entity->getUrl();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tape entity.');
        }

        if (!$this->getRequest()->get('page')) {
            $entity->setVisits($entity->getVisits()+1);
            $em->flush();
            $pageCarrent = 1;
        } else {
            $pageCarrent = $this->getRequest()->get('page');
        }
        if ($rss = simplexml_load_file($url)) {
            $news = $rss->xpath('//item');
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($news,
            $this->get('request')->query->get('page', 1), 5);
        $page = ceil(count($news) / 5);
        return array('tapes' => $pagination, 'pageCarrent' => $pageCarrent, 'page' => $page, 'id' => $id );
    }


    /**
     * @Route("/news/load/{category}", name="news_load")
     * @Method("GET")
     * @Template("RssNewsBundle:News:load.html.twig")
     */
    public function loadAction($category)
    {
        $em = $this->getDoctrine()->getManager();
        $tapes = $em->getRepository('RssNewsBundle:Tape')->findByActive(true);
        if (!$tapes) {
            throw $this->createNotFoundException('Unable to find Tape entity.');
        }
        //Search news by selected category
        $news = array();
        foreach ($tapes as $tape) {
            /** @var $rssService \Rss\NewsBundle\RssService\FormService*/
            $rssService = $this->get('rss_form');
            if ($rssService->checkRss($tape->getUrl())) {
                $rss = simplexml_load_file($tape->getUrl());
                $tapeCategories = $em->getRepository('RssNewsBundle:TapeCategory')->findByTape($tape->getId());
                if (!empty($tapeCategories)) {
                    $write = false;
                    foreach ($tapeCategories as $tapeCategory) {
                        // Load all item from Rss tape if tape have selected category
                        if ($tapeCategory->getCategory()->getTitle() === $category) {
                            $news = array_merge($rss->xpath('//item'),$news);
                            $write = true;
                            break;
                        }
                    }
                } else {
                    $write = false;
                }
                // Load item with selected category from Rss tape
                if (!$write) {
                    foreach ($rss->channel->item as $new) {
                        if ($new->category == $category) array_push($news,$new);
                    }
                }
            }
        }
        $pageCarrent = 1;
        if ($this->getRequest()->get('page')) {
            $pageCarrent = $this->getRequest()->get('page');
        }
        $page = ceil(count($news) / 5);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $news,
            $this->get('request')->query->get('page', 1), 5);
        return array('rss' => $pagination, 'pageCarrent' => $pageCarrent, 'page' => $page, 'category' => $category);
    }
}
