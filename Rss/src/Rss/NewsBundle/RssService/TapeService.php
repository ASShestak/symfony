<?php
namespace Rss\NewsBundle\RssService;
use Rss\NewsBundle\Entity\TapeCategory;
use Symfony\Component\DependencyInjection\ContainerInterface;
class TapeService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function loadCategory()
    {
        $em = $this->container->get('doctrine')->getManager();
        $entities = $em->getRepository('RssNewsBundle:Tape')->findByActive(1);
        $categories = array();
        foreach ($entities as $entity) {
            $url = $entity->getUrl();
            $entities = $em->getRepository('RssNewsBundle:TapeCategory')->findByTape($entity->getId());
            if (!empty($entities)) {
                foreach ($entities as $value) {
                    array_push($categories, $value->getCategory()->getTitle());
                }
            }
            libxml_use_internal_errors(true);
            $rss = simplexml_load_file($url);
            if ($rss)
                foreach ($rss->channel->item as $item)
                    if ($item->category) {
                        array_push($categories,$item->category);
                    }
        }
        $categories = array_unique($categories);
        return $categories;
    }

    public function saveCategory($categories, $entityTape)
    {
        $em = $this->container->get('doctrine')->getManager();
        $selectedCategories = explode('|',$categories);
        foreach ($selectedCategories as $value) {
            $entityCategory = $em->getRepository('RssNewsBundle:Category')->find($value);
            $tapeCategory = new TapeCategory();
            $tapeCategory->setCategory($entityCategory);
            $tapeCategory->setTape($entityTape);
            $em->persist($tapeCategory);
            $em->flush();
        }
    }

    public function deleteCategory($id)
    {
        $em = $this->container->get('doctrine')->getManager();
        $entities = $em->getRepository('RssNewsBundle:TapeCategory')->findByTape($id);
            foreach ($entities as $entity) {
                $em->remove($entity);
                $em->flush();
            }
    }
}