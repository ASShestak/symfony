<?php
namespace Rss\NewsBundle\RssService;
use Rss\NewsBundle\Entity\Tape;
use Rss\NewsBundle\Form\TapeType;
use Rss\NewsBundle\Entity\Category;
use Rss\NewsBundle\Form\CategoryType;
use Rss\NewsBundle\Entity\User;
use Rss\NewsBundle\Form\UserType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FormService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    //create TapeForm for create or edit rss
    public function createTapeForm(Tape $tape)
    {
        if (null === $tape->getId()) {
            $parameters = array(
                'action' =>$this->container->get('router')->generate('tape_new')
            );
            $label = 'Create';
        } else {
            $parameters = array(
                'action' => $this->container->get('router')->generate('tape_edit', array('id' => $tape->getId())),
            );
            $label = 'Update';
        }

        $form = $this->container->get('form.factory')->create(new TapeType(), $tape, $parameters);
        return $form->add('submit', 'submit', array('label' => $label,'attr'=>array('class'=>"btn btn-inverse")));
    }

    //check rss url in TapeForm
    public function checkRss( $file)
    {
        $heads = @get_headers($file);
        if (is_array($heads) && strpos($heads[0], '200')) {
            libxml_use_internal_errors(true);

            $rss = simplexml_load_file($file);
            if ($rss && $rss->xpath('//rss') && $rss->xpath('//channel') &&
                $rss->xpath('//item') && $rss->xpath('//title') &&
                $rss->xpath('//description') && $rss->xpath('//link')
                )
                return true;
        }
        $this->container->get('session')->getFlashbag()->add(
                'error_message',
                'RssLink is bad.'
            );
        return false;
    }

    //create form for create or edit category
    public function createCategoryForm(Category $category)
    {
        if (null === $category->getId()) {
            $parameters = array(
                'action' => $this->container->get('router')->generate('category_new'),
            );
            $label = 'Create';
        } else {
            $parameters = array(
                'action' => $this->container->get('router')->generate('category_edit', array('id' => $category->getId())),
            );
            $label = 'Update';
        }

        $form = $this->container->get('form.factory')->create(new CategoryType(), $category, $parameters);

        return $form->add('submit', 'submit', array('label' => $label,'attr'=>array('class'=>"btn btn-inverse")));
    }

    //create form for user
    public function createUserForm(User $user)
    {
        $parameters = array(
            'action' => $this->container->get('router')->generate('user_new')
            );

        $form = $this->container->get('form.factory')->create(new UserType(), $user, $parameters);

        return $form->add('submit', 'submit', array('label' => 'Create','attr'=>array('class'=>"btn btn-inverse")));
    }

}